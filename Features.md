Features
======================

##Current Features

  1. Administration

      *Ability to check a player's bank

      *Ability to check a player's balance

      *Ability to give/take money from players

  2. API Features

      *Vault Integration

      *Plugin Metrics

      *Standard API

      *Custom Events

  3. Auto Saving

      *Ability to enable/disable

      *Ability to set custom save interval

  4. Shops

      *Ability to create shops to sell and/or buy items.

      *Complete control

      *Set max shops a single player can own

      *Give players access to shop profit sharing

      *Ability to offer trades for all shop items

      *Ability to access shops via signs

      *Ability to white and/or blacklist players from your shop

      *Ability to hide your shop to only whitelisted players

      *Ability to have admin shops, which are able to have items with unlimited stock

  5. Banks

      *Ability to enable/disable

      *Ability to have multiple players per bank.

      *Ability to access via signs or a command

      *Ability to deposit and withdraw blocks and items

      *Ability to deposit and withdraw money

      *Saves bank item locations

      *Ability to have bank balances gain interest

      *Ability to enable/disable interest gaining

      *Ability to set custom interest rates

      *Ability to set a custom interval for interest

  6. Currency System

      *Singular and Plural Name Support

      *Ability to check your balance

      *Ability to give players money

      *Ability to pay people money from your own balance

      *Ability to have items as currency

      *Ability to have different currencies per-world

      *Ability to have a major and minor currency

  7. Language Support

      *Ability to change the default TNE messages

  8. Mob Rewards

      *Ability to reward the player money for killing a certain mob

      *Ability to enable/disable mob rewards for individual mobs

  9. Multiple Database Support

      *MySQL

      *H2

      *Regular FlatFile

  10. Multi-World Support

      *Ability to set configurations per world

      *Ability to charge people to change worlds

      *Ability to have balances set per-world

      *Ability to have banks set per-world

  11. Minecraft UUID Support

      *The New Economy keeps your users' data even if they change their username!

  12. MISC

      *Update Checker

      *Ability to disable UUID support

      *Ability to allow worlds to share economy data

      *Ability to allow the use of bank balances for transactions

      *Ability to charge for command use

      *Inventory Use Charging

        1. Ability to charge per use

        2. Ability to create packages to charge per second of use

      *Ability to reward/charge for using items

      *Ability to reward/charge for crafting

      *Ability to reward/charge for smelting

      *Ability to reward/charge for enchanting

      *Ability to reward/charge for mining

##Planned Features

###Version Unknown

  1. Currency Improvements

    *Ability to have tiered currencies

  2. Auctions

  3. Lotteries

  4. Administration command additions

    *Ability to modify configurations in-game

  5. Companies

    *Ability to have custom jobs

    *Ability to set custom paychecks

  6. Shop Signs

  7. Custom Plot Areas

    *Ability to buy, sell, trade and auction plots

    *Different types(Bank, Company, Personal, Shop, Trade)

  8. Direct Player Trading(Alternative to Sign Trading)
