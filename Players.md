Players.yml
==============

The default Players file
```
#Used for per-player configurations.
Players:

    #The username field could be the player's username or UUID.
    Username:
        #Now we Simply override and configurations we want for this specific player.
        #This allows us even more control over our server.
        Shops:
            Enabled: false
```
